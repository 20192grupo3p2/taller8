all: bin/estatico bin/dinamico

bin/estatico: obj/main.o lib/libdateutils.a
	gcc -g -static  obj/main.o -o bin/estatico -Llib -ldateutils

bin/dinamico: obj/main.o lib/libdateutils.so
	gcc -g obj/main.o -o bin/dinamico -Llib -ldateutils

obj/main.o: src/main.c
	gcc -Wall -I include/ -c $^ -o obj/main.o

obj/dias_estatico.o: src/dias.c
	gcc -Wall -I include/ -c $^ -o obj/dias_estatico.o

obj/dias_dinamico.o: src/dias.c
	gcc -Wall -I include/ -c $^ -o obj/dias_dinamico.o

obj/segundos_estatico.o: src/segundos.c
	gcc -Wall -I include/ -c $^ -o obj/segundos_estatico.o

obj/segundos_dinamico.o: src/segundos.c
	gcc -Wall -I include/ -c $^ -o obj/segundos_dinamico.o

obj/fecha_estatico.o: src/fecha.c
	gcc -Wall -I include/ -c $^ -o obj/fecha_estatico.o

obj/fecha_dinamico.o: src/fecha.c
	gcc -Wall -I include/ -c $^ -o obj/fecha_dinamico.o

lib/libdateutils.a: obj/dias_estatico.o obj/segundos_estatico.o obj/fecha_estatico.o
	ar rcs lib/libdateutils.a $^

lib/libdateutils.so: obj/dias_dinamico.o obj/segundos_dinamico.o obj/fecha_dinamico.o
	gcc -shared -fPIC -o lib/libdateutils.so $^

.PHONY: clean
clean:
	rm bin/* obj/* lib/*


